package gui;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import ticTacToe.BoardListener;
import ticTacToe.HighscoreList;
import ticTacToe.TicTacToeBoard;

/**
 * This class creates the GUI representing the board.
 * 
 * @author Fredrik23
 *
 */
public class BoardGUI {
	private JFrame frame;
	private String P1Name;
	private String P2Name;
	private String currentPlayer;
	private ImageIcon marker1;
	private ImageIcon marker2;
	private ImageIcon smallMarker1;
	private ImageIcon smallMarker2;
	private JPanel topPane;
	private JLabel turnLabel;
	private JLabel turnIcon;
	private TicTacToeBoard game;
	private int nbrOfPlayers;
	private JButton[][] buttons;
	private HighscoreList highscore;
	private int size;

	/**
	 * This constructor creates the board view and set up an environment with
	 * playerNames and icons which is used when the game is played. It also
	 * creates a TicTacToe object which represents the actual board.
	 * 
	 * @param size
	 *            - Size of the board
	 * @param frame
	 *            - the main frame
	 * @param P1Name
	 *            - name of player 1
	 * @param P2Name
	 *            - name of player 2
	 * @param marker1
	 *            - marker of player 1
	 * @param marker2
	 *            - marker of player 2
	 * @param numberOfPlayers
	 *            - 1 if singleplayer, 2 if multiplayer
	 */
	public BoardGUI(int size, JFrame frame, String P1Name, String P2Name,
			ImageIcon marker1, ImageIcon marker2, int numberOfPlayers, HighscoreList highscore) {
		frame.getContentPane().removeAll();
		game = new TicTacToeBoard(size);
		// Creates smaller version of the marker used for showing whose turn it
		// is
		Image image = marker1.getImage();
		Image newimg = image.getScaledInstance(20, 20,
				java.awt.Image.SCALE_SMOOTH);
		smallMarker1 = new ImageIcon(newimg);
		image = marker2.getImage();
		newimg = image.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		smallMarker2 = new ImageIcon(newimg);

		if (game.getCurrentPlayer() == TicTacToeBoard.P1) {
			currentPlayer = P1Name;
			turnIcon = new JLabel(smallMarker1);
		} else {
			currentPlayer = P2Name;
			turnIcon = new JLabel(smallMarker2);
		}
		nbrOfPlayers = numberOfPlayers;
		this.frame = frame;
		this.P1Name = P1Name;
		this.P2Name = P2Name;
		this.marker1 = marker1;
		this.marker2 = marker2;
		this.highscore = highscore;
		this.size = size;
		buttons = new JButton[size][size];
		frame.getContentPane().removeAll();
		JPanel contentPane = new JPanel();
	    topPane = new JPanel();
		JPanel buttonPane = new JPanel();
		JPanel boardPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		topPane.setLayout(new BoxLayout(topPane, BoxLayout.LINE_AXIS));
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		boardPane.setLayout(new GridLayout(size, size, 0, 0));
		turnLabel = new JLabel(currentPlayer + "'s turn  ");
		turnLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 15));
		topPane.add(turnLabel);
		topPane.add(turnIcon);
		// Creates a grid of buttons as the board
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				JButton button = new JButton();
				buttons[i][j] = button;
				button.setName(String.valueOf(i) + String.valueOf(j));
				button.addActionListener(new BoardListener(game, this,
						nbrOfPlayers));
				boardPane.add(button);
			}
		}
		MenuButton restartButton = new MenuButton("Restart");
		restartButton.addActionListener(new RestartListener());
		MenuButton backButton = new MenuButton("Back to Menu");
		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new MenuGUI(frame);
			}

		});

		buttonPane.add(restartButton);
		buttonPane.add(backButton);
		contentPane.add(topPane);
		contentPane.add(boardPane);
		contentPane.add(buttonPane);
		frame.setContentPane(contentPane);

		frame.pack();
		frame.repaint();
		frame.setVisible(true);
		//Draw a first mark if the game is singleplayer and the computer starts.
		if(numberOfPlayers==1 && game.getCurrentPlayer()==TicTacToeBoard.P2){			
			
			update(game.drawRandomMark());
			game.changePlayer();
			turnLabel.setText(P1Name+"'s turn  ");
			currentPlayer = P1Name;
			
		}

	}

	/**
	 * Updates the board view and the current player after a move is made.
	 * 
	 * @param index
	 *            - The index of the cell (button) marked by the move.
	 */
	public void update(String index) {
		JButton b = buttons[Character.getNumericValue(index.charAt(0))][Character
				.getNumericValue(index.charAt(1))];
		JLabel icon;
		if (game.getCurrentPlayer() == TicTacToeBoard.P1) {
			b.setDisabledIcon(marker1);
			b.setIcon(marker1);
			icon = new JLabel(smallMarker2);
			currentPlayer = P2Name;
		} else {
			b.setDisabledIcon(marker2);
			b.setIcon(marker2);
			icon = new JLabel(smallMarker1);
			currentPlayer = P1Name;
		}
		b.setEnabled(false);
		turnLabel.setText(currentPlayer + "'s turn  ");
		topPane.removeAll();
		topPane.add(turnLabel);
		topPane.add(icon);
	}

	/**
	 * Proclaims the winner in a Dialog window.
	 * 
	 * @param winner
	 *            - player that wins.
	 */
	public void proclaimWinner(int winner) {

		inactivateBoard();
		if (winner == TicTacToeBoard.P1) {
			highscore.updateScore(P1Name);
			JOptionPane.showMessageDialog(frame, P1Name + " wins!", "Winner!",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			highscore.updateScore(P2Name);
			JOptionPane.showMessageDialog(frame, P2Name + " wins!", "Winner!",
					JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	public void proclaimDraw(){
		inactivateBoard();
		JOptionPane.showMessageDialog(frame, " It's a draw!", "Draw!",
				JOptionPane.PLAIN_MESSAGE);
	}
	
	private void inactivateBoard(){
		 for(int i=0; i<size; i++){
			 for(int j=0; j<size; j++){
				 buttons[i][j].setEnabled(false);
			 } 
		 }
	}

	/**
	 * Used for resetting the board and view when the restart button is pressed.
	 */
	private class RestartListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new BoardGUI(3, frame, P1Name, P2Name, marker1, marker2,
					nbrOfPlayers, highscore);

		}

	}

}
