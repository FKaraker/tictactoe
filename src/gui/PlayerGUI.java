package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import ticTacToe.HighscoreList;

/**
 * Class that creates the gui that lets the players choose their names and
 * markers. It collects this information and passes it along to the BoardGUI.
 * 
 * @author Fredrik23
 *
 */

public class PlayerGUI {

	private JFrame frame;
	private int numberOfPlayers;
	private String playerName1;
	private ImageIcon marker1;
	private ImageIcon marker2;
	private JLabel headerLabel;
	private JTextField nameField;
	private JButton crossButton;
	private JButton circleButton;
	private JButton starButton;
	private JButton skullButton;
	private ImageIcon cross;
	private ImageIcon circle;
	private ImageIcon star;
	private ImageIcon skull;
	private ImageIcon demon;
	private boolean player1Ready;

	public PlayerGUI(JFrame frame, int numberOfPlayers) {
		this.frame = frame;
		player1Ready = false;
		this.numberOfPlayers = numberOfPlayers;

		createGUI();

	}

	private void createGUI() {
		frame.getContentPane().removeAll();
		JPanel contentPane = new JPanel();
		JPanel namePane = new JPanel();
		JPanel markPane = new JPanel();
		JPanel buttonPane = new JPanel();
		Color background = new Color(175, 219, 150);
		contentPane.setBackground(background);
		namePane.setBackground(background);
		buttonPane.setBackground(background);
		markPane.setBackground(background);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		headerLabel = new JLabel("Player 1");
		headerLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 25));
		CustomLabel nameLabel = new CustomLabel("Choose a name: ");
		nameField = new JTextField(15);
		nameField.setEditable(true);
		JLabel markLabel = new CustomLabel("Choose a marker: ");
		markLabel.setHorizontalAlignment(SwingConstants.LEFT);

		crossButton = new JButton();
		circleButton = new JButton();
		starButton = new JButton();
		skullButton = new JButton();

		// Loads icon images used for the markers from files.
		try {
			Image crossImg = ImageIO.read(getClass().getResource(
					"/resources/cross.png"));
			Image circleImg = ImageIO.read(getClass().getResource(
					"/resources/circle.png"));
			Image starImg = ImageIO.read(getClass().getResource(
					"/resources/star.png"));
			Image skullImg = ImageIO.read(getClass().getResource(
					"/resources/skull.png"));
			Image demonImg = ImageIO.read(getClass().getResource(
					"/resources/demon.png"));
			cross = new ImageIcon(crossImg);
			circle = new ImageIcon(circleImg);
			star = new ImageIcon(starImg);
			skull = new ImageIcon(skullImg);
			demon = new ImageIcon(demonImg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		crossButton.setIcon(cross);
		circleButton.setIcon(circle);
		starButton.setIcon(star);
		skullButton.setIcon(skull);

		MarkListener markList = new MarkListener();
		crossButton.addActionListener(markList);
		circleButton.addActionListener(markList);
		starButton.addActionListener(markList);
		skullButton.addActionListener(markList);

		MenuButton continueButton = new MenuButton("Continue");
		MenuButton backButton = new MenuButton("Back to Menu");
		backButton.addActionListener(new BackListener());
		continueButton.addActionListener(new ContinueListener());

		namePane.add(nameLabel);
		namePane.add(nameField);
		markPane.add(crossButton);
		markPane.add(circleButton);
		markPane.add(starButton);
		markPane.add(skullButton);
		buttonPane.add(backButton);
		buttonPane.add(Box.createRigidArea(new Dimension(20, 0)));
		buttonPane.add(continueButton);

		contentPane.add(headerLabel);
		contentPane.add(Box.createRigidArea(new Dimension(0, 70)));
		contentPane.add(namePane);
		contentPane.add(markLabel);
		contentPane.add(markPane);
		buttonPane.setAlignmentX(Box.BOTTOM_ALIGNMENT);
		contentPane.add(buttonPane);
		contentPane.add(Box.createRigidArea(new Dimension(0, 70)));
		frame.setContentPane(contentPane);
		nameField.setSize(30, 10);
		frame.repaint();
		frame.setVisible(true);

	}

	/**
	 * Called when the first player is finished with the settings if the game is
	 * multiplayer. It disables the marker that player 1 chose and resets the
	 * name field.
	 */

	private void secondPlayerGUI() {
		headerLabel.setText("Player 2");
		nameField.setText("");

		if (marker1.equals(cross)) {
			crossButton.setEnabled(false);
			// crossButton.setForeground(disabled);
		}
		if (marker1.equals(circle)) {
			circleButton.setEnabled(false);
			// circleButton.setForeground(disabled);
		}
		if (marker1.equals(star)) {
			starButton.setEnabled(false);
			// starButton.setForeground(disabled);
		}
		if (marker1.equals(skull)) {
			skullButton.setEnabled(false);
			// skullButton.setForeground(disabled);
		}

	}

	/**
	 * Listener that listens to the marker buttons.
	 *
	 */
	private class MarkListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton b = (JButton) e.getSource();
			if (!player1Ready) {
				marker1 = (ImageIcon) b.getIcon();
				System.out.println("mark set!");

			} else {
				marker2 = (ImageIcon) b.getIcon();
			}

		}

	}

	/**
	 * Listener that listens to the back to menu button.
	 *
	 */

	private class BackListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new MenuGUI(frame);

		}

	}

	/**
	 * Listens to the continue button and starts the BoardGUI. It also checks
	 * that the player has entered a name and chosen a marker, and that the
	 * players have different names. If not all of this is true, it creates a
	 * message dialog telling the player what must be done.
	 */

	private class ContinueListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String name = nameField.getText();
			if (name.isEmpty()) {
				JOptionPane.showMessageDialog(frame, "You must enter a name!",
						"TicTacToe", JOptionPane.PLAIN_MESSAGE);
				return;
			}
			if (player1Ready && name.equals(playerName1)) {
				JOptionPane.showMessageDialog(frame,
						"You can not choose the same name as player 1!",
						"TicTacToe", JOptionPane.PLAIN_MESSAGE);
				return;
			}
			if (!player1Ready && marker1 == null) {
				JOptionPane.showMessageDialog(frame,
						"You must choose a marker!", "TicTacToe",
						JOptionPane.PLAIN_MESSAGE);
				return;
			}
			if (player1Ready && marker2 == null) {
				JOptionPane.showMessageDialog(frame,
						"You must choose a marker!", "TicTacToe",
						JOptionPane.PLAIN_MESSAGE);
				return;
			}
			if (numberOfPlayers == 2 && !player1Ready) {
				playerName1 = name;
				player1Ready = true;
				secondPlayerGUI();
			} else if (numberOfPlayers == 2) {

				new BoardGUI(3, frame, playerName1, name, marker1, marker2, 2,
						new HighscoreList());
			} else {
				new BoardGUI(3, frame, name, "Computer", marker1, demon, 1,
						new HighscoreList());

			}

		}

	}

}
