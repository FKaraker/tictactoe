package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class creates the Highscore panel which is then shown in the menu.
 * @author Fredrik23
 *
 */

public class HighscorePanel extends JPanel {
	
	private static final long serialVersionUID = 5862799795696656955L;

	public HighscorePanel(ArrayList<String> names, ArrayList<Integer> scores){
		super();
		JPanel listPane = new JPanel();
		JPanel labelPane = new JPanel();
		JPanel headerPane = new JPanel();
		JPanel marginPane = new JPanel();
		Color background = new Color(175, 219,150);
		marginPane.setLayout(new BoxLayout(marginPane, BoxLayout.LINE_AXIS));
		marginPane.setBackground(background);
		headerPane.setBackground(background);
		labelPane.setBackground(background);
		listPane.setBackground(background);
		this.setBackground(background);
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		headerPane.setLayout(new BoxLayout(headerPane, BoxLayout.LINE_AXIS));
		JLabel headerLabel = new JLabel("Highscore");
		headerLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 25));
		headerPane.add(headerLabel);
		labelPane.setLayout(new BoxLayout(labelPane, BoxLayout.LINE_AXIS));
		JLabel highscoreLabel = new JLabel("Highscore"); 
		highscoreLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 25));
	    CustomLabel nameLabel = new CustomLabel("Name");
	    highscoreLabel.setAlignmentY(CENTER_ALIGNMENT);;
	    CustomLabel scoreLabel = new CustomLabel("Wins");
	    labelPane.add(nameLabel);
	    labelPane.add(Box.createRigidArea(new Dimension(80,0)));
	    labelPane.add(scoreLabel);
	    this.add(Box.createRigidArea(new Dimension(0,10)));
	    this.add(headerPane);
	    this.add(Box.createRigidArea(new Dimension(0,10)));
	     if(names.size()==0){
	    	 listPane.setLayout(new BoxLayout(listPane, BoxLayout.LINE_AXIS));
	    	 JLabel label = new JLabel("No highscores yet...");
	    	 listPane.add(Box.createRigidArea(new Dimension(20,0)));
	    	 listPane.add(label);
	    	  this.add(labelPane);
	    	  this.add(Box.createRigidArea(new Dimension(0,30)));
	     }else{
	    	 this.add(labelPane);
	    	 this.add(Box.createRigidArea(new Dimension(0,10)));
	      listPane.setLayout(new GridLayout(5,2));
	      int i =0;
	     while(i<5 && i<names.size()){
		     listPane.add(new JLabel(names.get(i)));
		     listPane.add(new JLabel(""+scores.get(i)));
		     i++;
		     
		     }
	     if(i<3){
	    	 listPane.add(new JLabel(""));
	    	 listPane.add(new JLabel(""));
	    	 listPane.add(new JLabel(""));
	    	 listPane.add(new JLabel(""));
	    	 
	     }
	     marginPane.add(Box.createRigidArea(new Dimension(105,0)));
    	 
	     }
	     marginPane.add(listPane);	   
	     this.add(marginPane);
	 
	     
	}

}
