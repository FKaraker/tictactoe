package gui;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JButton;


/**
 * A button with a certain font used in the menu.
 * @author Fredrik23
 *
 */
public class MenuButton extends JButton {

	private static final long serialVersionUID = -4502443295537060703L;

	public MenuButton(String text){
		this.setText(text);
		this.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		this.setAlignmentX(Component.CENTER_ALIGNMENT);	
	
	}

}
