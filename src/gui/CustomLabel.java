package gui;

import java.awt.Font;

import javax.swing.JLabel;

/**
 * A label used in the GUI to avoid having to set the properties over and over
 * again.
 * 
 * @author Fredrik23
 *
 */

public class CustomLabel extends JLabel {

	private static final long serialVersionUID = -4481028588524507490L;

	public CustomLabel(String text) {
		super(text);
		this.setFont(new Font("Britannic Bold", Font.PLAIN, 15));
	}

}
