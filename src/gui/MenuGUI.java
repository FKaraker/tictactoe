package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import ticTacToe.HighscoreList;

/**
 * Creates the gui for the first menu.
 * 
 * @author Fredrik23
 *
 */

public class MenuGUI {

	private JFrame frame;

	public MenuGUI(JFrame frame) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.frame = frame;
		frame.getContentPane().removeAll();
		Container menuPane = new JPanel();
		Container buttonPane = new JPanel();
		HighscoreList highscoreList = new HighscoreList();
		JPanel highscorePane = new HighscorePanel(highscoreList.getNames(),
				highscoreList.getScores());
		menuPane.setLayout(new GridLayout(3, 1));
		highscorePane.setBackground(new Color(175, 219, 150));
		buttonPane.setBackground(new Color(175, 219, 150));
		menuPane.setBackground(new Color(175, 219, 150));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JFrame.setDefaultLookAndFeelDecorated(true);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		JButton singleButton = new MenuButton("Single Player");
		singleButton.addActionListener(new SingleButtonListener());
		JButton multiButton = new MenuButton("Multiplayer");
		multiButton.addActionListener(new MultiButtonListener());

		buttonPane.add(Box.createRigidArea(new Dimension(22, 10)));
		buttonPane.add(singleButton);
		buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
		buttonPane.add(multiButton);
		menuPane.add(highscorePane);
		menuPane.add(buttonPane);
		Container contentPane = frame.getContentPane();
		contentPane.setBackground(new Color(175, 219, 150));
		contentPane.add(menuPane, BorderLayout.PAGE_START);

		frame.pack();
		frame.setSize(new Dimension(350, 350));
		frame.setMinimumSize(new Dimension(360, 360));
		frame.setMaximumSize(new Dimension(360, 360));
		Dimension buttonDim = new Dimension(150, 150);
		singleButton.setMinimumSize(buttonDim);
		multiButton.setMinimumSize(buttonDim);
		frame.repaint();
		frame.setVisible(true);

	}

	/**
	 * Used for creating the next part of the giu when the "singleplayer" button
	 * is pressed.
	 *
	 */
	private class SingleButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new PlayerGUI(frame, 1);

		}

	}

	/**
	 * Used for creating the next part of the giu when the "multiplayer" button
	 * is pressed.
	 *
	 */

	private class MultiButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new PlayerGUI(frame, 2);

		}

	}
}
