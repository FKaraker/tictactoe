package ticTacToe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Represents the highscore list and writes it to file.
 * 
 * @author Fredrik23
 *
 */

public class HighscoreList {

	private ArrayList<HighscoreEntry> list;

	/**
	 * Reads the highscore list from a file and puts it in an ArrayList.
	 */

	public HighscoreList() {
		list = new ArrayList<HighscoreEntry>();
		BufferedReader reader = null;
		String name;
		int score;
		try {
			reader = new BufferedReader(new FileReader("highscores.txt"));
			while ((name = reader.readLine()) != null) {
				System.out.println(name);
				score = Integer.parseInt(reader.readLine());
				System.out.println(score);
				list.add(new HighscoreEntry(name, score));
			}
		} catch (FileNotFoundException e) {
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Used for updating the list when somebody wins.
	 * 
	 * @param name
	 *            - Name of the player.
	 */

	public void updateScore(String name) {
		boolean matchFound = false;
		for (HighscoreEntry entry : list) {
			if (name.equals(entry.getName())) {
				entry.increaseScore();
				matchFound = true;
				System.out.println("match found!");
			}
		}
		if (!matchFound) {
			list.add(new HighscoreEntry(name, 1));
			System.out.println("match not found!");
		}

		Collections.sort(list);
		Collections.reverse(list);

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(
					"highscores.txt"));
			for (HighscoreEntry entry : list) {
				writer.write(entry.getName());
				writer.write("\n");
				int score = entry.getScore();
				writer.write(Integer.toString(score));
				writer.write("\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @return An arrayList of all the names in the list.
	 */

	public ArrayList<String> getNames() {
		ArrayList<String> result = new ArrayList<String>();
		for (HighscoreEntry entry : list) {
			result.add(entry.getName());
		}
		return result;
	}

	/**
	 * @return An arrayList of all the scores in the list.
	 */

	public ArrayList<Integer> getScores() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (HighscoreEntry entry : list) {
			result.add(entry.getScore());
		}
		return result;

	}

	/**
	 * A class that represents an entry in the highscore list. It implements
	 * Comparable which makes it easy to order the list.
	 * 
	 */

	private class HighscoreEntry implements Comparable<Object> {
		private int score;
		private String name;

		public HighscoreEntry(String name, int score) {
			this.name = name;
			this.score = score;
		}

		public String getName() {
			return name;
		}

		public int getScore() {
			return score;
		}

		public void increaseScore() {
			score++;
		}

		@Override
		public int compareTo(Object o) {
			if (o instanceof HighscoreEntry) {
				HighscoreEntry h2 = (HighscoreEntry) o;
				int score2 = h2.getScore();
				if (score < score2) {
					return -1;
				}
				if (score == score2) {
					return 0;
				} else {
					return -1;
				}
			} else {
				throw new ClassCastException();
			}
		}

	}

}
