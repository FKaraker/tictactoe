package ticTacToe;

import gui.BoardGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 * A Listener class that listens to all the cells of the board and updates the
 * virtual board if a mark is set by the user. It then checks if there is a
 * winner, and in that case tells the GUI.
 * 
 * @author Fredrik23
 *
 */

public class BoardListener implements ActionListener {

	private BoardGUI gui;
	private TicTacToeBoard board;
	private int nbrOfPlayers;

	public BoardListener(TicTacToeBoard board, BoardGUI gui, int nbrOfPlayers) {
		this.gui = gui;
		this.board = board;
		this.nbrOfPlayers = nbrOfPlayers;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		// Updates the gui so it can draw a mark in the correct button (cell).
		gui.update(b.getName());
		String buttonID = b.getName();
		// The name of the button is used for determining which cell the button
		// represents. The first character of the name is the row and the second
		// the column.
		board.drawMark(Character.getNumericValue(buttonID.charAt(0)),
				Character.getNumericValue(buttonID.charAt(1)));

		if (board.checkForWinner()) {
			gui.proclaimWinner(board.getCurrentPlayer());
			return;
		}
		if (board.boardFull()) {
			gui.proclaimDraw();
			return;
		}

		board.changePlayer();

		// If the game is singleplayer the following code draws the computers
		// mark.

		if (nbrOfPlayers == 1) {

			String index = board.drawRandomMark();
			gui.update(index);
			if (board.checkForWinner()) {
				gui.proclaimWinner(board.getCurrentPlayer());
				return;
			}
			board.changePlayer();

			if (board.boardFull()) {
				gui.proclaimDraw();
				return;
			}

		}

	}

}
