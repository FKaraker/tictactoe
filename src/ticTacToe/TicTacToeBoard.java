package ticTacToe;

import java.util.Random;

/**
 * Class that represents a TicTacToe board with methods.
 * 
 * @author Fredrik23
 *
 */

public class TicTacToeBoard {
	private int[][] board;
	private int currentPlayer;
	private int nbrOfTurns;
	public static int EMPTY = 0;
	public static int P1 = 1;
	public static int P2 = 2;
	public int size;

	public TicTacToeBoard(int size) {
		this.size = size;
		board = new int[size][size];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				board[i][j] = 0;
			}
		}
		// Randomly chooses the player to start the game.
		Random rand = new Random();
		currentPlayer = rand.nextInt(2) + 1;
		nbrOfTurns = 0;
	}

	/**
	 * Checks the entire board for 3 marks of the same player in a row.
	 * 
	 * @return True if a winner is found, else false.
	 */

	public boolean checkForWinner() {
		if (rowCheck() || colCheck() || diagCheck()) {
			return true;
		}
		return false;
	}

	/**
	 * Checks each row for a winner.
	 */

	public boolean rowCheck() {
		for (int i = 0; i < 3; i++) {
			if (compareCells(board[0][i], board[1][i], board[2][i]) == true) {
				return true;

			}

		}

		return false;

	}

	/**
	 * Checks each column for a winner.
	 */

	public boolean colCheck() {
		for (int i = 0; i < 3; i++) {
			if (compareCells(board[i][0], board[i][1], board[i][2]) == true) {
				return true;

			}

		}

		return false;

	}

	/**
	 * Checks the two diagonals for a winner.
	 */

	public boolean diagCheck() {
		if (compareCells(board[0][0], board[1][1], board[2][2])
				|| compareCells(board[2][0], board[1][1], board[0][2])) {
			return true;
		}
		return false;

	}

	/**
	 * Local method for comparing 3 cells.
	 * 
	 * @return True if all the cells are marked the same, else false.
	 */

	private boolean compareCells(int c1, int c2, int c3) {
		if (c1 != EMPTY && c1 == c2 && c2 == c3) {
			return true;
		}
		return false;
	}

	/**
	 * Draws a mark on the board in the specified cell and changes the current
	 * player.
	 * 
	 * @param row
	 * @param col
	 */
	public void drawMark(int row, int col) {
		board[row][col] = currentPlayer;
		nbrOfTurns++;

	}

	public String drawRandomMark() {
		Random rand = new Random();
		int row;
		int col;
		while (true) {
			row = rand.nextInt(size);
			col = rand.nextInt(size);
			int cell = board[row][col];
			if (cell == 0) {
				drawMark(row, col);
				return Integer.toString(row) + col;
			}
		}
	}

	/**
	 * returns the current player.
	 */

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Changes the current player.
	 */

	public void changePlayer() {
		if (currentPlayer == P1) {
			currentPlayer = P2;
		} else {
			currentPlayer = P1;
		}

	}

	/**
	 * Checks if the board is full.
	 * 
	 * @return True if full, else false.
	 */

	public boolean boardFull() {
		return nbrOfTurns >= (size * size);
	}

}
