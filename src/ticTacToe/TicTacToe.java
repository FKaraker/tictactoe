package ticTacToe;

import javax.swing.JFrame;
import gui.MenuGUI;

/**
 * Main class that starts the GUI for the menu. 
 * @author Fredrik23
 *
 */

public class TicTacToe {

	public static void main(String[] args) {
		JFrame frame = new JFrame("TicTacToe");
		new MenuGUI(frame);

	}

}
